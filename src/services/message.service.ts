import { IMessage, IUserTokenized } from "../interfaces";

export const createMessage = (
  message: string,
  user: IUserTokenized
): IMessage => {
  return {
    _id: Date.now().toString(),
    user,
    text: message,
    createdAt: new Date().toISOString(),
  };
};

export const updateMessage = (messages: IMessage[], message: IMessage) => {
  const updatedMessageId = messages.findIndex((m) => m._id === message._id);

  if (updatedMessageId !== -1) {
    return messages
      .slice(0, updatedMessageId)
      .concat(message, messages.slice(updatedMessageId + 1));
  }

  return messages;
};

export const deleteMessage = (messages: IMessage[], messageId: string) => {
  return messages.filter((message) => message._id !== messageId);
};

export const editMessage = (message: IMessage, text: string) => {
  return { ...message, text, editedAt: new Date().toISOString() };
};

export const likeMessage = (messages: IMessage[], messageId: string) => {
  const message = messages.find((message) => message._id === messageId);

  if (!message) {
    return;
  }

  return { ...message, liked: !message.liked };
};
