export const setLocalStorageItem = (key: string, value: any): void => {
  let validValue: string;

  if (typeof value === "object") {
    validValue = JSON.stringify(value);
  } else {
    validValue = String(value);
  }

  localStorage.setItem(key, validValue);
};

export const getObjectFromLocalStorage = (key: string): any => {
  const data = localStorage.getItem(key);

  if (!data) {
    return null;
  }

  return JSON.parse(data);
};
