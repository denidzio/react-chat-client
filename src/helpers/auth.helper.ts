import { IUserTokenized } from "../interfaces";
import {
  setLocalStorageItem,
  getObjectFromLocalStorage,
} from "./localStorage.helper";

export const setTokenizedUser = (credentials: IUserTokenized | null) => {
  setLocalStorageItem("user", credentials);
};

export const getTokenizedUser = (): IUserTokenized | null => {
  return getObjectFromLocalStorage("user");
};

export const getAuthHeader = (token: string) => {
  return {
    Authorization: `Bearer ${token}`,
  };
};
