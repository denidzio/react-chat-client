import jwt_decode from "jwt-decode";
import {
  ICredentials,
  IError,
  IToken,
  IUser,
  IUserTokenized,
} from "../interfaces";
import { custom } from "./common.api";

const entity = "auth";

const login = async (
  credentials: ICredentials
): Promise<IUserTokenized | undefined> => {
  try {
    const response = await custom(`${entity}/login`, "POST", credentials);
    const data: IUserTokenized & IError = response.data;

    if (data.error) {
      alert(`Error. ${data.message}`);
      return;
    }

    const token: IToken = jwt_decode(data.token);
    const user = {
      _id: token.id,
      name: data.name,
      roles: data.roles,
      token: data.token,
    };

    return user;
  } catch (e) {
    console.log(e);
    throw new Error(e);
  }
};

const register = async (user: IUser): Promise<IUser | undefined> => {
  try {
    const response = await custom(`${entity}/register`, "POST", user);
    const data: IUser & IError = response.data;

    if (data.error) {
      alert(`Error. ${data.message}`);
      return;
    }

    return response.data;
  } catch (e) {
    console.log(e);
    throw new Error(e);
  }
};

export { login as loginApi, register as registerApi };
