import { getAuthHeader } from "../helpers/auth.helper";
import { IError, IMessage } from "../interfaces";
import { getAll } from "./common.api";

const entity = "message";

const getAllMessages = async (token: string): Promise<IMessage[]> => {
  try {
    const response = await getAll(entity, getAuthHeader(token));
    const data: IMessage[] & IError = response.data;

    if (data.error) {
      alert(`Error. ${data.message}`);
      return [];
    }

    return data;
  } catch (e) {
    console.log(e);
    throw new Error(e);
  }
};

export { getAllMessages as getAllMessagesApi };
