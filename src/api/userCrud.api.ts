import { getAuthHeader } from "../helpers/auth.helper";
import { IError, IUser } from "../interfaces";
import { deleteReq, getAll, update } from "./common.api";

const entity = "user";

const getAllUsers = async (token: string): Promise<IUser[]> => {
  try {
    const response = await getAll(entity, getAuthHeader(token));
    const data: IUser[] & IError = response.data;

    if (data.error) {
      alert(`Error. ${data.message}`);
      return [];
    }

    return data;
  } catch (e) {
    console.log(e);
    throw new Error(e);
  }
};

const updateUser = async (
  userId: string,
  user: IUser,
  token: string
): Promise<IUser | undefined> => {
  try {
    const response = await update(entity, userId, user, getAuthHeader(token));
    const data: IUser & IError = response.data;

    if (data.error) {
      alert(`Error. ${data.message}`);
      return;
    }

    return data;
  } catch (e) {
    console.log(e);
    throw new Error(e);
  }
};

const deleteUser = async (
  userId: string,
  token: string
): Promise<IUser | undefined> => {
  try {
    const response = await deleteReq(entity, userId, getAuthHeader(token));
    const data = response.data;

    if (data.error) {
      alert(`Error. ${data.message}`);
      return;
    }

    return data;
  } catch (e) {
    console.log(e);
    throw new Error(e);
  }
};

export {
  getAllUsers as getAllUsersApi,
  updateUser as updateUserApi,
  deleteUser as deleteUserApi,
};
