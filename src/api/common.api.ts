import axios from "axios";
import { Method } from "axios/";
import { DEVELOPMENT } from "../config";

export const SOCKET = DEVELOPMENT
  ? "http://localhost:3002"
  : `${window.origin}`;

export const API = DEVELOPMENT
  ? "http://localhost:3002/api"
  : `${window.origin}/api`;

export const getAll = async (entityName: string, headers = {}) => {
  try {
    return await axios.get(`${API}/${entityName}`, { headers });
  } catch (error) {
    return error.response;
  }
};

export const getOne = async (entityName: string, id: string, headers = {}) => {
  try {
    return await axios.get(`${API}/${entityName}/${id}`, { headers });
  } catch (error) {
    return error.response;
  }
};

export const create = async <T>(entityName: string, body: T, headers = {}) => {
  try {
    return await axios.post(`${API}/${entityName}`, body, { headers });
  } catch (error) {
    return error.response;
  }
};

export const update = async <T>(
  entityName: string,
  id: string,
  body: T,
  headers = {}
) => {
  try {
    return await axios.put(`${API}/${entityName}/${id}`, body, { headers });
  } catch (error) {
    return error.response;
  }
};

export const deleteReq = async (
  entityName: string,
  id: string,
  headers = {}
) => {
  try {
    return await axios.delete(`${API}/${entityName}/${id}`, { headers });
  } catch (error) {
    return error.response;
  }
};

export const custom = async (
  path: string,
  method: Method,
  body = {},
  headers = {}
) => {
  try {
    if (["GET", "DELETE"].includes(method)) {
      return await axios.request({ method, url: `${API}/${path}`, headers });
    }

    return await axios.request({
      method,
      url: `${API}/${path}`,
      data: body,
      headers: headers,
    });
  } catch (error) {
    return error.response;
  }
};
