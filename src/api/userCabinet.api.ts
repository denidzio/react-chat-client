import { getAuthHeader } from "../helpers/auth.helper";
import { IError, IMessage } from "../interfaces";
import { custom } from "./common.api";

const entity = "user";

const sendMessage = async (
  message: string,
  token: string
): Promise<IMessage | undefined> => {
  try {
    const response = await custom(
      `${entity}/message`,
      "POST",
      {
        text: message,
      },
      getAuthHeader(token)
    );
    const data: IMessage & IError = response.data;

    if (data.error) {
      alert(`Error. ${data.message}`);
      return;
    }

    return data;
  } catch (e) {
    console.log(e);
    throw new Error(e);
  }
};

const editMessage = async (
  messageId: string,
  message: string,
  token: string
): Promise<IMessage | undefined> => {
  try {
    const response = await custom(
      `${entity}/message/${messageId}`,
      "PUT",
      {
        text: message,
      },
      getAuthHeader(token)
    );
    const data = response.data;

    if (data.error) {
      alert(`Error. ${data.message}`);
      return;
    }

    return data;
  } catch (e) {
    console.log(e);
    throw new Error(e);
  }
};

const likeMessage = async (
  messageId: string,
  token: string
): Promise<IMessage | undefined> => {
  try {
    const response = await custom(
      `${entity}/message/${messageId}`,
      "PATCH",
      {},
      getAuthHeader(token)
    );
    const data = response.data;

    if (data.error) {
      alert(`Error. ${data.message}`);
      return;
    }

    return data;
  } catch (e) {
    console.log(e);
    throw new Error(e);
  }
};

const deleteMessage = async (
  messageId: string,
  token: string
): Promise<IMessage | undefined> => {
  try {
    const response = await custom(
      `${entity}/message/${messageId}`,
      "DELETE",
      {},
      getAuthHeader(token)
    );
    const data = response.data;

    if (data.error) {
      alert(`Error. ${data.message}`);
      return;
    }

    return data;
  } catch (e) {
    console.log(e);
    throw new Error(e);
  }
};

export {
  sendMessage as sendMessageApi,
  editMessage as editMessageApi,
  likeMessage as likeMessageApi,
  deleteMessage as deleteMessageApi,
};
