interface IUserTokenized {
  _id: string;
  name: string;
  roles: string[];
  token: string;
}

export default IUserTokenized;
