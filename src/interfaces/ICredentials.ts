interface ICredentials {
  name: string;
  password: string;
}

export default ICredentials;
