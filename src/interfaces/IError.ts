interface IErrorResponse {
  error?: string;
  message?: string;
}

export default IErrorResponse;
