interface IToken {
  id: string;
  exp: number;
}

export default IToken;
