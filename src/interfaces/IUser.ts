interface IUser {
  _id?: string;
  name: string;
  avatar?: string;
  password?: string;
}

export default IUser;
