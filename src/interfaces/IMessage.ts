import IUser from "./IUser";

interface IMessage {
  _id?: string;
  user: IUser;
  text: string;
  liked?: boolean;
  createdAt?: string;
  modifiedAt?: string;
}

export default IMessage;
