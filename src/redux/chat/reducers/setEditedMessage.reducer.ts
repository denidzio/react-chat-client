import { IChatStore } from "../interfaces";

const setEditedMessageReducer = (state: IChatStore, action: any) => {
  state.editedMessage = action.payload;
};

export default setEditedMessageReducer;
