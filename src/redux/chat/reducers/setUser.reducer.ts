import { IChatStore } from "../interfaces";

const setUserReducer = (state: IChatStore, action: any) => {
  state.user = action.payload;
};

export default setUserReducer;
