import { likeMessage, updateMessage } from "../../../services/message.service";
import { IChatStore } from "../interfaces";

const likeMessageReducer = (state: IChatStore, action: any) => {
  const likedMessage = likeMessage(state.messages, action.payload);

  if (!likedMessage) {
    return;
  }

  state.messages = updateMessage(state.messages, likedMessage);
};

export default likeMessageReducer;
