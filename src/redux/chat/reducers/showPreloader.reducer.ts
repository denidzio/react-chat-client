import { IChatStore } from "../interfaces";

const showPreloaderReducer = (state: IChatStore) => {
  state.preloader = true;
};

export default showPreloaderReducer;
