import { IChatStore } from "../interfaces";

const hidePreloaderReducer = (state: IChatStore) => {
  state.preloader = false;
};

export default hidePreloaderReducer;
