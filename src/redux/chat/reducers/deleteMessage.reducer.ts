import { IChatStore } from "../interfaces";
import { deleteMessage } from "../../../services/message.service";

const deleteMessageReducer = (state: IChatStore, action: any) => {
  state.messages = deleteMessage(state.messages, action.payload);
};

export default deleteMessageReducer;
