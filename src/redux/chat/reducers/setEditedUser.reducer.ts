import { IChatStore } from "../interfaces";

const setEditedMessageReducer = (state: IChatStore, action: any) => {
  state.editedUser = action.payload;
};

export default setEditedMessageReducer;
