import { IChatStore } from "../interfaces";
import {
  updateMessage,
  editMessage as editMessageService,
} from "../../../services/message.service";

const editMessageReducer = (state: IChatStore, action: any) => {
  if (!state.editedMessage) {
    return;
  }

  const editedMessage = editMessageService(state.editedMessage, action.payload);
  state.messages = updateMessage(state.messages, editedMessage);
};

export default editMessageReducer;
