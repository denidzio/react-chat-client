import { createMessage } from "../../../services/message.service";
import { IChatStore } from "../interfaces";

const sendMessageReducer = (state: IChatStore, action: any) => {
  if (!state.user) {
    return;
  }

  const message = createMessage(action.payload, state.user);
  state.messages = [...state.messages, message];
};

export default sendMessageReducer;
