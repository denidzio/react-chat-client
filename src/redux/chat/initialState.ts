import { IChatStore } from "./interfaces";

const initialState: IChatStore = {
  messages: [],
  users: [],
  preloader: false,
};

export default initialState;
