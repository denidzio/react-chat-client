import { IMessage, IUser, IUserTokenized } from "../../../interfaces";

interface IChatStore {
  messages: IMessage[];
  users: IUser[];
  preloader: boolean;
  user?: IUserTokenized;
  editedUser?: IUser;
  editedMessage?: IMessage;
}

export default IChatStore;
