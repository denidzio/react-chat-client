import { IChatStore } from ".";

interface IState {
  state: { chat: IChatStore };
}

export default IState;
