import { createAsyncThunk } from "@reduxjs/toolkit";
import { updateUserApi } from "../../../api/userCrud.api";
import { IUser } from "../../../interfaces";

import { IState } from "../interfaces";

const updateUserThunk = createAsyncThunk<void, IUser, IState>(
  "chat/updateUser",
  async (user, { getState }) => {
    const token = getState().chat.user?.token;
    const userId = getState().chat.editedUser?._id;
    token && userId && (await updateUserApi(userId, user, token));
  }
);

export default updateUserThunk;
