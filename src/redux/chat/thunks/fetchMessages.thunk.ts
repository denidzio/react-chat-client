import { createAsyncThunk } from "@reduxjs/toolkit";
import { getAllMessagesApi } from "../../../api/messageCrud.api";
import { IMessage } from "../../../interfaces";
import { IState } from "../interfaces";

const fetchMessagesThunk = createAsyncThunk<IMessage[], undefined, IState>(
  "chat/fetchMessages",
  async (_, { getState }) => {
    const token = getState().chat.user?.token;
    return token ? await getAllMessagesApi(token) : [];
  }
);

export default fetchMessagesThunk;
