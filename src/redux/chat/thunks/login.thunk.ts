import { createAsyncThunk } from "@reduxjs/toolkit";
import { loginApi } from "../../../api/auth.api";
import { ICredentials } from "../../../interfaces";

const loginThunk = createAsyncThunk(
  "chat/login",
  async (credentials: ICredentials) => {
    return await loginApi(credentials);
  }
);

export default loginThunk;
