import { createAsyncThunk } from "@reduxjs/toolkit";
import { likeMessageApi } from "../../../api/userCabinet.api";
import { IState } from "../interfaces";
import { likeMessage } from "../slice";

const likeMessageThunk = createAsyncThunk<void, string, IState>(
  "chat/likeMessage",
  async (messageId: string, { getState, dispatch }) => {
    dispatch(likeMessage(messageId));
    const token = getState().chat.user?.token;
    token && likeMessageApi(messageId, token);
  }
);

export default likeMessageThunk;
