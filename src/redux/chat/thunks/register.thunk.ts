import { createAsyncThunk } from "@reduxjs/toolkit";
import { registerApi } from "../../../api/auth.api";
import { IUser } from "../../../interfaces";

const registerThunk = createAsyncThunk("chat/register", async (data: IUser) => {
  await registerApi(data);
});

export default registerThunk;
