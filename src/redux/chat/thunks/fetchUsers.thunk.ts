import { createAsyncThunk } from "@reduxjs/toolkit";
import { getAllUsersApi } from "../../../api/userCrud.api";
import { IUser } from "../../../interfaces";
import { IState } from "../interfaces";

const fetchUsersThunk = createAsyncThunk<IUser[], undefined, IState>(
  "chat/fetchUsers",
  async (_, { getState }) => {
    const token = getState().chat.user?.token;
    return token ? await getAllUsersApi(token) : [];
  }
);

export default fetchUsersThunk;
