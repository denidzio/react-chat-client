import { createAsyncThunk } from "@reduxjs/toolkit";
import { deleteMessageApi } from "../../../api/userCabinet.api";
import { IState } from "../interfaces";
import { deleteMessage } from "../slice";

const deleteMessageThunk = createAsyncThunk<void, string, IState>(
  "chat/deleteMessage",
  async (messageId: string, { getState, dispatch }) => {
    dispatch(deleteMessage(messageId));
    const token = getState().chat.user?.token;
    token && (await deleteMessageApi(messageId, token));
  }
);

export default deleteMessageThunk;
