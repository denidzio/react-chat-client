import { createAsyncThunk } from "@reduxjs/toolkit";
import { deleteUserApi } from "../../../api/userCrud.api";
import { IState } from "../interfaces";

const deleteUserThunk = createAsyncThunk<void, string, IState>(
  "chat/deleteUser",
  async (userId, { getState }) => {
    const token = getState().chat.user?.token;
    token && (await deleteUserApi(userId, token));
  }
);

export default deleteUserThunk;
