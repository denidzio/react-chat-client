import { createAsyncThunk } from "@reduxjs/toolkit";
import { sendMessage } from "../slice";
import { sendMessageApi } from "../../../api/userCabinet.api";
import { IState } from "../interfaces";

const sendMessageThunk = createAsyncThunk<void, string, IState>(
  "chat/sendMessage",
  async (message: string, { getState, dispatch }) => {
    dispatch(sendMessage(message));

    const token = getState().chat.user?.token;
    token && sendMessageApi(message, token);
  }
);

export default sendMessageThunk;
