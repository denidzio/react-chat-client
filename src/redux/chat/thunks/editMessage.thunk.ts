import { createAsyncThunk } from "@reduxjs/toolkit";
import { editMessageApi } from "../../../api/userCabinet.api";
import { IState } from "../interfaces";
import { editMessage } from "../slice";

const editMessageThunk = createAsyncThunk<void, string, IState>(
  "chat/editMessage",
  async (message: string, { getState, dispatch }) => {
    dispatch(editMessage);

    const msg = getState().chat.editedMessage;
    const token = getState().chat.user?.token;

    msg && msg._id && token && (await editMessageApi(msg._id, message, token));
  }
);

export default editMessageThunk;
