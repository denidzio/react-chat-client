import { createSlice } from "@reduxjs/toolkit";
import { loginThunk, fetchUsersThunk, fetchMessagesThunk } from "./thunks";
import initialState from "./initialState";
import { setTokenizedUser } from "../../helpers/auth.helper";
import { IMessage, IUser, IUserTokenized } from "../../interfaces";
import * as chatReducer from "./reducers";

const name = "chat";

export const chatSlice = createSlice({
  name,
  initialState,
  reducers: {
    showPreloader: chatReducer.showPreloader,
    hidePreloader: chatReducer.hidePreloader,
    setUser: chatReducer.setUser,
    setEditedMessage: chatReducer.setEditedMessage,
    setEditedUser: chatReducer.setEditedUser,
    sendMessage: chatReducer.sendMessage,
    editMessage: chatReducer.editMessage,
    likeMessage: chatReducer.likeMessage,
    deleteMessage: chatReducer.deleteMessage,
  },
  extraReducers(builder) {
    builder.addCase(loginThunk.fulfilled, (state, action) => {
      action.payload && setTokenizedUser(action.payload);
      state.user = action.payload;
    });
    builder.addCase(fetchUsersThunk.fulfilled, (state, action) => {
      state.users = action.payload;
    });
    builder.addCase(fetchMessagesThunk.fulfilled, (state, action) => {
      state.messages = action.payload;
    });
  },
});

export const {
  showPreloader,
  hidePreloader,
  setUser,
  setEditedMessage,
  setEditedUser,
  sendMessage,
  editMessage,
  likeMessage,
  deleteMessage,
} = chatSlice.actions;

export const selectPreloader = (state: any) => state.chat.preloader;
export const selectEditModal = (state: any) => state.chat.editModal;
export const selectEditedMessage = (state: any): IMessage =>
  state.chat.editedMessage;
export const selectMessages = (state: any): IMessage[] => state.chat.messages;
export const selectUser = (state: any): IUserTokenized => state.chat.user;
export const selectUsers = (state: any): IUser[] => state.chat.users;
export const selectEditedUser = (state: any): IUser => state.chat.editedUser;

export default chatSlice.reducer;
