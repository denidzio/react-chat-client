import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { Switch, Route, Redirect, useLocation } from "react-router-dom";
import { Navigation, Preloader } from "./components";
import { getTokenizedUser } from "./helpers/auth.helper";
import { setUser } from "./redux/chat/slice";
import { Chat, Auth, MessageEditor, Users, UserEditor } from "./screens/";

function App() {
  const location = useLocation();
  const dispatch = useDispatch();
  const [isLoad, setIsLoad] = useState(false);

  useEffect(() => {
    const tokenizedUser = getTokenizedUser();

    if (tokenizedUser) {
      dispatch(setUser(tokenizedUser));
    }

    setIsLoad(true);
  }, []);

  if (!isLoad) {
    return null;
  }

  return (
    <main className="app">
      <Preloader />
      <Switch>
        <Route path="/auth">
          <Auth />
        </Route>
        <Route path="/chat">
          <Chat />
        </Route>
        <Route path="/message-editor">
          <MessageEditor />
        </Route>
        <Route path="/users">
          <Users />
        </Route>
        <Route path="/user-editor">
          <UserEditor />
        </Route>
        <Route path="/">
          <Redirect to="/auth" />
        </Route>
      </Switch>
      {location.pathname !== "/auth" ? <Navigation /> : null}
    </main>
  );
}

export default App;
