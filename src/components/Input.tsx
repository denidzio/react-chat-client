import React, { ChangeEvent } from "react";

function Input({
  id = "",
  type = "text",
  value = "",
  className = "",
  placeHolder = "",
  required = false,
  onChange = (e: ChangeEvent<HTMLInputElement>) => {},
}) {
  return (
    <label htmlFor={id} className={`label form__label ${className}`}>
      <input
        value={value}
        type={type}
        className="input"
        id={id}
        onChange={onChange}
        placeholder={placeHolder}
        required={required}
      />
    </label>
  );
}

export default Input;
