import React, { ReactNode } from "react";

function Button({
  children,
  className = "",
}: {
  children: ReactNode;
  className?: string;
}) {
  return (
    <button type="submit" className={`button ${className}`}>
      {children}
    </button>
  );
}

export default Button;
