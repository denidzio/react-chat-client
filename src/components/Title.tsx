import React, { ReactNode } from "react";

function Title({
  children,
  className = "",
}: {
  children: ReactNode;
  className?: string;
}) {
  return <h2 className={`title ${className}`}>{children}</h2>;
}

export default Title;
