import React, { SyntheticEvent } from "react";
import { ReactNode } from "react";

function Form({
  children,
  className = "",
  onSubmit = (e: SyntheticEvent) => {},
}: {
  children: ReactNode;
  className: string;
  onSubmit: (e: SyntheticEvent) => void;
}) {
  return (
    <form className={`form ${className}`} onSubmit={onSubmit}>
      {children}
    </form>
  );
}

export default Form;
