import React, { Fragment, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { Role } from "../enums";
import { setTokenizedUser } from "../helpers/auth.helper";
import { selectUser, setUser } from "../redux/chat/slice";

function Navigation() {
  const dispatch = useDispatch();
  const user = useSelector(selectUser);

  const handleLogout = useCallback(() => {
    setTokenizedUser(null);
    dispatch(setUser(null));
  }, []);

  return (
    <ul className="navigation">
      <li className="navigation-item navigation__item">
        <NavLink to="/chat">Chat</NavLink>
      </li>
      {user && user.roles.includes(Role.ADMIN) ? (
        <Fragment>
          <li className="navigation-item navigation__item">
            <NavLink to="users">User list</NavLink>
          </li>
          <li className="navigation-item navigation__item">
            <NavLink to="/user-editor">User editor</NavLink>
          </li>
        </Fragment>
      ) : null}
      <li className="navigation-item navigation__item" onClick={handleLogout}>
        Logout
      </li>
    </ul>
  );
}

export default Navigation;
