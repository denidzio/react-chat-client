import React from "react";
import { useSelector } from "react-redux";
import { selectPreloader } from "../redux/chat/slice";

function Preloader() {
  const loader = useSelector(selectPreloader);

  if (!loader) {
    return null;
  }

  return (
    <div className="preloader">
      <div className="lds-dual-ring"></div>
    </div>
  );
}

export default Preloader;
