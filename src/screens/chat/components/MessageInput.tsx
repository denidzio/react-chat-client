import React, {
  useState,
  useCallback,
  useRef,
  ChangeEvent,
  SyntheticEvent,
} from "react";
import { useDispatch } from "react-redux";
import { sendMessageThunk } from "../../../redux/chat/thunks";

function MessageInput() {
  const dispatch = useDispatch();
  const inputRef = useRef<HTMLInputElement>(null);
  const [message, setMessage] = useState("");

  const handleInput = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    setMessage(e.currentTarget.value);
  }, []);

  const handleSubmit = useCallback(
    (e: SyntheticEvent) => {
      e.preventDefault();

      if (!message.trim()) {
        return;
      }

      dispatch(sendMessageThunk(message));
      setMessage("");
    },
    [message, dispatch]
  );

  return (
    <form className="message-input" onSubmit={handleSubmit}>
      <label htmlFor="message" className="message-input-label">
        <input
          ref={inputRef}
          onChange={handleInput}
          value={message}
          type="text"
          className="message-input-text"
          id="message"
          placeholder="Enter your message..."
          autoComplete="off"
          spellCheck={false}
        />
      </label>
      <button
        type="submit"
        className="message-input-button message-input__button"
      >
        Send
      </button>
    </form>
  );
}

export default MessageInput;
