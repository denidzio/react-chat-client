import React from "react";

import { IMessage } from "../../../interfaces";
import { getDiffDays, getDividerText } from "../../../services/chat.service";

function Divider({
  currentMsg,
  prevMsg,
}: {
  currentMsg: IMessage;
  prevMsg: IMessage;
}) {
  if (!currentMsg.createdAt) {
    return null;
  }

  if (
    prevMsg &&
    prevMsg.createdAt &&
    getDiffDays(currentMsg.createdAt, prevMsg.createdAt) === 0
  ) {
    return null;
  }

  return (
    <div className="messages-divider">
      {getDividerText(currentMsg.createdAt)}
    </div>
  );
}

export default Divider;
