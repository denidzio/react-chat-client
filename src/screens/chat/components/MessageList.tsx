import React, {
  Fragment,
  useState,
  useEffect,
  useCallback,
  useRef,
} from "react";
import { useDispatch, useSelector } from "react-redux";

import {
  selectMessages,
  selectUser,
  setEditedMessage,
} from "../../../redux/chat/slice";
import { Message, Divider, OwnMessage } from ".";
import { Redirect } from "react-router-dom";

function MessageList() {
  const listRef = useRef<HTMLDivElement>(null);

  const dispatch = useDispatch();

  const messages = useSelector(selectMessages);
  const user = useSelector(selectUser);

  const [redirect, setRedirect] = useState(false);
  const [messagesCount, setMessagesCount] = useState(messages.length);

  const scrollToBottom = useCallback(() => {
    const node = listRef.current;

    if (!node) {
      return;
    }

    node.scrollTop = node.scrollHeight;
  }, []);

  const preventScroll = useCallback((e: KeyboardEvent) => {
    if (e.key === "ArrowUp") {
      e.preventDefault();
    }
  }, []);

  const handleKeyUp = useCallback(
    (e: KeyboardEvent) => {
      if (e.key !== "ArrowUp") {
        return;
      }

      const lastMessage = messages
        .filter((message) => message.user._id === user._id)
        .reverse()[0];

      dispatch(setEditedMessage(lastMessage));
      setRedirect(true);
    },
    [messages, user, dispatch]
  );

  useEffect(() => {
    window.addEventListener("keyup", handleKeyUp);
    window.addEventListener("keydown", preventScroll);

    if (messagesCount !== messages.length) {
      scrollToBottom();
    }

    return () => {
      window.removeEventListener("keyup", handleKeyUp);
      window.removeEventListener("keypress", preventScroll);
    };
  }, [handleKeyUp, preventScroll]);

  useEffect(() => {
    if (messages.length > messagesCount) {
      scrollToBottom();
    }

    setMessagesCount(messages.length);
  }, [messages, messagesCount]);

  if (!user) {
    return null;
  }

  if (redirect) {
    return <Redirect to="/message-editor" />;
  }

  return (
    <div className="message-list" ref={listRef}>
      {messages.map((message, index) => (
        <Fragment key={message._id}>
          <Divider currentMsg={message} prevMsg={messages[index - 1]} />
          {message.user._id === user._id ? (
            <OwnMessage message={message} />
          ) : (
            <Message message={message} />
          )}
        </Fragment>
      ))}
    </div>
  );
}

export default MessageList;
