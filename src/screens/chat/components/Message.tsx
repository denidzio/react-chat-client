import React, { useCallback } from "react";
import { useDispatch } from "react-redux";
import { IMessage } from "../../../interfaces";
import { likeMessageThunk } from "../../../redux/chat/thunks";
import { getMessageTime } from "../../../services/chat.service";

function Message({ message }: { message: IMessage }) {
  const dispatch = useDispatch();

  const handleLikeMessage = useCallback(() => {
    if (!message._id) {
      return;
    }

    dispatch(likeMessageThunk(message._id));
  }, [message, dispatch]);

  return (
    <div className="message chat__message">
      <div className="message-user-avatar">
        <img src={message.user.avatar} alt="" />
      </div>
      <div className="message-body message__body">
        <div className="message-body-header message-body__header">
          <div className="message-user-name">{message.user.name}</div>
          <div className="message-time message-body-header__time">
            {message.createdAt ? getMessageTime(message.createdAt) : null}
          </div>
          <div className="message-heart">
            <svg
              onClick={handleLikeMessage}
              className={message.liked ? "message-liked" : "message-like"}
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 512 512"
            >
              <path d="M462.3 62.6C407.5 15.9 326 24.3 275.7 76.2L256 96.5l-19.7-20.3C186.1 24.3 104.5 15.9 49.7 62.6c-62.8 53.6-66.1 149.8-9.9 207.9l193.5 199.8c12.5 12.9 32.8 12.9 45.3 0l193.5-199.8c56.3-58.1 53-154.3-9.8-207.9z"></path>
            </svg>
          </div>
        </div>
        <p className="message-text">{message.text}</p>
      </div>
    </div>
  );
}

export default Message;
