import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import { Socket, io } from "socket.io-client";
import { DefaultEventsMap } from "socket.io-client/build/typed-events";
import { SOCKET } from "../../api/common.api";
import {
  hidePreloader,
  selectUser,
  showPreloader,
} from "../../redux/chat/slice";
import { fetchMessagesThunk } from "../../redux/chat/thunks";
import { Header, MessageList, MessageInput } from "./components";

function Chat() {
  const dispatch = useDispatch();
  const user = useSelector(selectUser);
  const [socket, setSocket] =
    useState<Socket<DefaultEventsMap, DefaultEventsMap>>();

  useEffect(() => {
    (async () => {
      if (!user) {
        return;
      }

      setSocket(io(SOCKET));

      dispatch(showPreloader());
      await dispatch(fetchMessagesThunk());
      dispatch(hidePreloader());
    })();
  }, []);

  useEffect(() => {
    if (!socket) {
      return;
    }

    socket.on("UPDATE_MESSAGES", () => {
      dispatch(fetchMessagesThunk());
    });
  }, [socket, dispatch]);

  if (!user) {
    return <Redirect to="/" />;
  }

  return (
    <div className="chat">
      <Header />
      <MessageList />
      <MessageInput />
    </div>
  );
}

export default Chat;
