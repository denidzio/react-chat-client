import React, {
  useState,
  useCallback,
  ChangeEvent,
  SyntheticEvent,
} from "react";
import { useDispatch } from "react-redux";
import { Button, Form, Input } from "../../../components";
import { ICredentials } from "../../../interfaces";
import { loginThunk } from "../../../redux/chat/thunks";

function AuthForm() {
  const dispatch = useDispatch();

  const [name, setName] = useState("");
  const [password, setPassword] = useState("");

  const handleInputName = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    setName(e.currentTarget.value);
  }, []);

  const handleInputPassword = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      setPassword(e.currentTarget.value);
    },
    []
  );

  const handleSubmit = useCallback(
    async (e: SyntheticEvent) => {
      e.preventDefault();

      if (!name.trim() || !password.trim()) {
        return;
      }

      const credentials: ICredentials = {
        name: name.trim(),
        password: password.trim(),
      };

      await dispatch(loginThunk(credentials));
    },
    [name, password, dispatch]
  );

  return (
    <Form className="auth-form" onSubmit={handleSubmit}>
      <Input
        value={name}
        onChange={handleInputName}
        placeHolder="Login"
        id="login"
        required
      />
      <Input
        value={password}
        onChange={handleInputPassword}
        type="password"
        placeHolder="Password"
        id="password"
        required
      />
      <Button>Sign in</Button>
    </Form>
  );
}

export default AuthForm;
