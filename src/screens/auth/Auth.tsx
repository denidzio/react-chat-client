import React from "react";
import { Redirect } from "react-router";
import { useSelector } from "react-redux";
import { AuthForm } from "./components";
import { Role } from "../../enums";
import { Title } from "../../components";
import { selectUser } from "../../redux/chat/slice";

function Auth() {
  const user = useSelector(selectUser);

  if (user) {
    return (
      <Redirect to={user.roles.includes(Role.ADMIN) ? "/users" : "/chat"} />
    );
  }

  return (
    <div className="auth">
      <Title className="auth__title">Authorization</Title>
      <AuthForm />
    </div>
  );
}

export default Auth;
