import React from "react";
import { useSelector } from "react-redux";
import { selectUsers } from "../../../redux/chat/slice";
import User from "./User";

function UsersList() {
  const users = useSelector(selectUsers);

  return (
    <ul className="users-list form">
      {users.map((user) => (
        <User user={user} key={user._id} className="users-list__user" />
      ))}
    </ul>
  );
}

export default UsersList;
