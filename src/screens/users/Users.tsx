import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import { Title } from "../../components";
import { Role } from "../../enums";
import {
  hidePreloader,
  selectUser,
  showPreloader,
} from "../../redux/chat/slice";
import { fetchUsersThunk } from "../../redux/chat/thunks";
import { UsersList } from "./components";

function Users() {
  const dispatch = useDispatch();
  const user = useSelector(selectUser);

  useEffect(() => {
    (async () => {
      dispatch(showPreloader());
      await dispatch(fetchUsersThunk());
      dispatch(hidePreloader());
    })();
  }, []);

  if (!user || !user.roles.includes(Role.ADMIN)) {
    return <Redirect to="/" />;
  }

  return (
    <div className="users">
      <Title className="users-title users__title">User list</Title>
      <UsersList />
    </div>
  );
}

export default Users;
