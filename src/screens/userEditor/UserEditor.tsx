import React from "react";
import { useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import { Title } from "../../components";
import { Role } from "../../enums";
import { selectUser } from "../../redux/chat/slice";
import { EditorForm } from "./components";

function UserEditor() {
  const user = useSelector(selectUser);

  if (!user || !user.roles.includes(Role.ADMIN)) {
    return <Redirect to="/" />;
  }

  return (
    <div className="user-editor">
      <Title className="user-editor__title">User editor</Title>
      <EditorForm />
    </div>
  );
}

export default UserEditor;
