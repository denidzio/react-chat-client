import React, {
  useState,
  useEffect,
  useCallback,
  ChangeEvent,
  SyntheticEvent,
} from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import { Button, Form, Input } from "../../../components";
import { IUser } from "../../../interfaces";
import { selectEditedUser, setEditedUser } from "../../../redux/chat/slice";
import {
  fetchUsersThunk,
  registerThunk,
  updateUserThunk,
} from "../../../redux/chat/thunks";

function EditorForm() {
  const dispatch = useDispatch();
  const editedUser = useSelector(selectEditedUser);

  const [redirect, setRedirect] = useState(false);

  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [avatar, setAvatar] = useState("");

  useEffect(() => {
    if (!editedUser) {
      return;
    }

    setName(editedUser.name);
    setAvatar(editedUser.avatar ? editedUser.avatar : "");
  }, [editedUser]);

  const handleInputName = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    setName(e.currentTarget.value);
  }, []);

  const handleInputPassword = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      setPassword(e.currentTarget.value);
    },
    []
  );

  const handleInputAvatar = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    setAvatar(e.currentTarget.value);
  }, []);

  const handleSubmit = useCallback(
    async (e: SyntheticEvent) => {
      e.preventDefault();

      if (!name.trim()) {
        return;
      }

      if (editedUser) {
        let updatedUser: IUser = {
          name,
          avatar,
        };

        if (password.trim()) {
          updatedUser.password = password.trim();
        }

        await dispatch(updateUserThunk(updatedUser));
        await dispatch(fetchUsersThunk());
        dispatch(setEditedUser(undefined));

        setRedirect(true);
        return;
      }

      if (!password.trim()) {
        return;
      }

      const request: IUser = {
        name,
        avatar,
        password,
      };

      await dispatch(registerThunk(request));
      await dispatch(fetchUsersThunk());

      setRedirect(true);
    },
    [name, password, avatar, editedUser, dispatch]
  );

  if (redirect) {
    return <Redirect to="/users" />;
  }

  return (
    <Form className="user-editor-form" onSubmit={handleSubmit}>
      <Input
        value={name}
        onChange={handleInputName}
        placeHolder="Login"
        required={true}
      />
      <Input
        value={avatar}
        onChange={handleInputAvatar}
        placeHolder="Avatar URL"
      />
      <Input
        value={password}
        onChange={handleInputPassword}
        placeHolder="Password"
        required={editedUser ? false : true}
      />
      <Button>{editedUser ? "Edit" : "Create"}</Button>
    </Form>
  );
}

export default EditorForm;
