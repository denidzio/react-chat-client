import React from "react";
import { useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import { Title } from "../../components";
import { selectEditedMessage } from "../../redux/chat/slice";
import { EditorForm } from "./components";

function MessageEditor() {
  const editedMessage = useSelector(selectEditedMessage);

  if (!editedMessage) {
    return <Redirect to="/" />;
  }

  return (
    <div className="message-editor">
      <Title className="message-editor__title">Message Editor</Title>
      <EditorForm />
    </div>
  );
}

export default MessageEditor;
