import React, {
  useState,
  useEffect,
  useCallback,
  ChangeEvent,
  SyntheticEvent,
} from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect, useHistory } from "react-router-dom";
import { Button, Form, Input } from "../../../components";
import { selectEditedMessage } from "../../../redux/chat/slice";
import { editMessageThunk } from "../../../redux/chat/thunks";

function EditorForm() {
  const dispatch = useDispatch();
  const editedMessage = useSelector(selectEditedMessage);

  const [redirect, setRedirect] = useState(false);
  const [message, setMessage] = useState("");

  useEffect(() => {
    setMessage(editedMessage.text);
  }, [editedMessage]);

  const handleInput = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    setMessage(e.currentTarget.value);
  }, []);

  const handleSubmit = useCallback(
    (e: SyntheticEvent) => {
      e.preventDefault();

      dispatch(editMessageThunk(message));
      setRedirect(true);
    },
    [message, dispatch]
  );

  if (redirect) {
    return <Redirect to="/chat" />;
  }

  return (
    <Form className="message-editor-form" onSubmit={handleSubmit}>
      <Input value={message} onChange={handleInput} />
      <Button>Edit</Button>
    </Form>
  );
}

export default EditorForm;
